import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, PopoverController, ToastController } from '@ionic/angular';
import { dbService } from '../../services/db.service';
import { AdMobFree, AdMobFreeInterstitialConfig} from '@ionic-native/admob-free/ngx';
declare var StartAppAds:any;

@Component({
  selector: 'app-showdb',
  templateUrl: './showdb.page.html',
  styleUrls: ['./showdb.page.scss'],
})

export class ShowdbPage implements OnInit {
  public text :string = '';
  public id = null;
  arr_of_scans =[];
  arr_of_id=[];
   image_result =[];
  constructor(private dbService: dbService,
    private nav_ctrl : NavController, private alert : AlertController,
    private popover : PopoverController,
    private toast : ToastController,
    private admobFree :AdMobFree,private modal : ModalController) {}

  ngOnInit() {
    StartAppAds.showInterstitial();
  // this.showInterstitialAds();
    this.loaddata();
  }
//   showInterstitialAds(){
//     try {
//       let interstitialConfig: AdMobFreeInterstitialConfig = {
//         isTesting: true, // Remove in production
//         autoShow: true,//,

//         //id: "ca-app-pub-3940256099942544/6300978111"
//          id:"ca-app-pub-4646433692422477/5706817120",
//     };
//     this.admobFree.interstitial.config(interstitialConfig);
//     this.admobFree.interstitial.prepare().then(() => {
//     }).catch(e => alert(e));
//     } catch (error) {
//       console.log(error);
//       this.presenttoast(error);
//     }


// }
 async presenttoast(msg){
 let show =  await this.toast.create({
   message : msg,
   duration: 2000,
 })
      show.present();
  }
  loaddata() {
    this.dbService.getAllEntriesOfTable('table_2').then((res) => {
      for (let i = 0; i < res.rows.length; i++) {
        let item = res.rows.item(i);
        // do something with it

        this.text = this.text + '\n' +  item.text;
     //   this.id = this.id +'\n' + item.id;

        this.arr_of_scans.push({text:item.text,id:item.id});
     //  this.image_result.push( this.id)


      }
    });
  }
  refresh(){

  }
  back(){
    //this.nav_ctrl.navigateForward('home');
    this.modal.dismiss();
  }
  async  delete(id){

      await this.alert.create({
       header: 'Are you sure to delete',
       message: 'You cannot reverse this scan...',
       buttons:[{
         text:'Cancel',
         role: 'cancel',

       },
     {
       text :'Delete',
       handler: () =>{
         this.dbService.deletescans('table_2',id).then(()=>{

         })
       }

       }]
     }).then(alert =>{
       alert.present();
     })
  }
  delteall(){
    this.dbService.deleteall().then(()=>{

    //  this.loaddata();
    })
  }
}
