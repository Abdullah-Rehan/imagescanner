import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowdbPageRoutingModule } from './showdb-routing.module';

import { ShowdbPage } from './showdb.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowdbPageRoutingModule
  ],
  declarations: [ShowdbPage]
})
export class ShowdbPageModule {}
