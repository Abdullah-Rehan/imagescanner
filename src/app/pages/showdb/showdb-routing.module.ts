import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShowdbPage } from './showdb.page';

const routes: Routes = [
  {
    path: '',
    component: ShowdbPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShowdbPageRoutingModule {}
