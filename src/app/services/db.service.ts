import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { AlertController, ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class dbService {

  constructor(private toast: ToastController,private alert : AlertController) { }
  createTable(name: string) {
    var sqlite = new SQLite();
    return sqlite.create({
      name: 'scan.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
//id INTEGER AUTOINCREMENT,
        console.log(`Created Table: ${name} `)
        this.presentToast('Value successfully save')
        return db.executeSql('CREATE TABLE IF NOT EXISTS table_2 (id INTEGER  PRIMARY KEY AUTOINCREMENT,text TEXT)', [])




      })
      .catch(e => {
        console.log(`error on Creating Table in ${name} with Err: ${e.message}`)
        return `error on Creating Table in ${name} with Err: ${e.message}` ;


      });
  }

  insertEntry(tableName: string, value: string, id: string) {
    var sqlite2 = new SQLite();


    return sqlite2.create({
      name: 'scan.db',
      location: 'default'

    }).then((db: SQLiteObject) => {
      console.log(`Inserten in Table: ${tableName} `)
      return db.executeSql(`INSERT INTO table_2 (text) VALUES ("${value}") ` , [])
      //let data = [id,value]
      // return db.executeSql('INSERT INTO table_1 (id,text) VALUES (?,?)', data);


    })
      .catch(e => {
        let msg = `error on Inserting  in ${tableName} with Err: ${e.message}`;
       this.presentToast(msg);
        console.log(`error on Inserting  in ${tableName} with Err: ${e.message}`)
        return `error on Inserting  in ${tableName} with Err: ${e.message}` ;
      });
  }


  getAllEntriesOfTable(tableName: string) {
    var sqlite2 = new SQLite();

    return sqlite2.create({
      name: 'scan.db',
      location: 'default'

    }).then((db: SQLiteObject) => {
      return db.executeSql(`SELECT * FROM table_2 `, [])

    })
      .catch(e => {
        let msg = `error on loading  in ${tableName} with Err: ${e.message}`;
        this.presentToast(msg);
        console.log(`error on Inserting  in ${tableName} with Err: ${e.message}`)
        return `error on loading  in ${tableName} with Err: ${e.message}` ;
      });
  }

 async presentToast(msg){
    let show = await this.toast.create({
      message : msg,
      duration :5000,
    });
    show.present();
  }
  // async showAlert(id){
  //    await this.alert.create({
  //     header: 'Are you sure to delete',
  //     message: 'You cannot reverse this scan...',
  //     buttons:[{
  //       text:'Cancel',
  //       role: 'cancel',

  //     },
  //   {
  //     text :'Delete',
  //     handler: () =>{
  //       this.deletescans('table_2',id)
  //     }

  //     }]
  //   }).then(alert =>{
  //     alert.present();
  //   })
  // }


  deletescans(tableName: string, id){

    var sqlite2 = new SQLite();

    return sqlite2.create({
      name: 'scan.db',
      location: 'default'

    }).then((db: SQLiteObject) => {
      return db.executeSql(`DELETE  FROM table_2 WHERE id = ? `, [id])

    })
      .catch(e => {
        let msg = `error on deleting  in ${tableName} with Err: ${e.message}`;
        this.presentToast(msg);
        console.log(`error on deleting  in ${tableName} with Err: ${e.message}`)
        return `error on deleting  in ${tableName} with Err: ${e.message}` ;
      });
  }
  deleteall(){
    var sqlite2 = new SQLite();
    return sqlite2.create({
      name:'scan.db',
      location:'default'
    }).then((db :SQLiteObject) =>{
      this.presentToast('deleted')
      return db.executeSql('DROP TABLE table_2')
    }).catch(e => {
      let msg = `error on deleting  in tabel with Err: ${e.message}`;
      this.presentToast(msg);
      console.log(`error on deleting  in tabel with Err: ${e.message}`)
      return `error on deleting  in tabel with Err: ${e.message}` ;
    });
  }

}
