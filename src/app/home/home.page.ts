import {
  Component,

} from '@angular/core';
import { PictureSourceType, Camera } from '@ionic-native/camera/ngx';
import { ActionSheetController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { OcrService } from '../services/ocr.service';
import { PhotoService } from '../services/photo.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { dbService } from '../services/db.service';
import { Router } from '@angular/router';
import {ShowdbPage} from '../pages/showdb/showdb.page';
import { AdMobFree, AdMobFreeBannerConfig} from '@ionic-native/admob-free/ngx';
declare var StartAppAds:any;
// @Directive({
//   selector: 'ion-textarea[autosize]'
// })

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{

  // @HostListener("input", ["$event.target"])
  // onInput = (textArea: HTMLTextAreaElement): void => {
  //   this.adjust();
  // }
  text: any = 'Output of the image will be shown here...';
  show_opt : boolean = false;
  show_save: boolean = false;
  selectedImage = '';
  image;
  //  @ViewChild('myInput') myInput: ElementRef;
  constructor(
    public actionSheetCtrl: ActionSheetController,
    // public camera: Camera,
    private photoService: PhotoService,
    private ocrService: OcrService,
    private camera: Camera,
 //   public element:ElementRef,
    public socailSharing: SocialSharing,
    public clipboard: Clipboard,
    private toast: ToastController,
    private databaseService: dbService,
    private nav_ctrl : NavController,
    private modal : ModalController,
    private admobFree : AdMobFree,
    private plt : Platform

  ) {
    this.plt.ready().then(()=>{
   // this.showBannerAd();
   this.showBanner();
    });
  }

  showBanner(){
    StartAppAds.showBanner();
//StartAppAds.hideBanner();
  }
//   showBannerAd() {
//     let bannerConfig: AdMobFreeBannerConfig = {
//         isTesting: true, // Remove in production
//         autoShow: true,
//         //id: "ca-app-pub-3940256099942544/6300978111"
//         id:"ca-app-pub-4646433692422477/4585307149",
//         // banner ca-app-pub-4646433692422477/4585307149
//     };
//     this.admobFree.banner.config(bannerConfig);

//     this.admobFree.banner.prepare().then(() => {
//         // success
//     }).catch(e => alert(e));
// }
  async takePicture() {
    //this.photoService.takePicture().then((text) => {
    let actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Use Library',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          },
        },
        {
          text: 'Capture Image',
          handler: () => {
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
        },
      ],
    });
    actionSheet.present();
  }

  getPicture(sourceType: PictureSourceType) {
    this.camera
      .getPicture({
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: sourceType,
        //allowEdit: true,
        saveToPhotoAlbum: false,
        correctOrientation: true,
      })
      .then((imageData) => {
        this.selectedImage = `data:image/jpeg;base64,${imageData}`;
        this.image = imageData;
        this.recognizeImage();
      });
  }

  //     this.ocrService.convertPicInText(text)
  //     .then (text => {
  //       this.text = text;
  //     })
  //   });
  // }
  recognizeImage() {
    this.ocrService.convertPicInText(this.image).then((text) => {
      this.text = text;
      this.show_opt =true;
      this.show_save =true;
    });
  }

  Share() {
    this.socailSharing.share(this.text);
  }
  copytoClip() {
    this.clipboard.copy(this.text);
    this.showtoast();
  }
  async showtoast() {
    let show = await this.toast.create({
      message: 'Copied to Clipboard',
      duration: 1500,
    });
    show.present();
  }
  shareviaemail() {
    this.socailSharing.shareViaEmail(this.text, null, null);
  }
  savetodb() {
    let id ;
    id= Date.now();
    this.databaseService.createTable('table_1').finally(() => {
      this.databaseService.insertEntry('table_1', this.text,id);
      this.show_save =false;
      //  this.text = "saved."
    });
  }
 async open_second_page(){
 //  this.nav_ctrl.navigateForward('showdb');
  let show = await  this.modal.create({
    component: ShowdbPage,
  })
  show.present();
  }
}
