import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
declare var StartAppAds:any;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private plt: Platform) {
    this.plt.ready().then(() =>{
      if(this.plt.is("android")){
        StartAppAds.init("202540825");
      }
    })
  }
}
