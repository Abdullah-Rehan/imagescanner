import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera } from '@ionic-native/camera/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import {SocialSharing} from '@ionic-native/social-sharing/ngx'
import {Clipboard} from '@ionic-native/clipboard/ngx';
import {SQLite} from '@ionic-native/sqlite/ngx'
import { AdMobFree } from '@ionic-native/admob-free/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),
    AppRoutingModule,

  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    Crop,
    SocialSharing,
    Clipboard,
    SQLite,
    AdMobFree,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
